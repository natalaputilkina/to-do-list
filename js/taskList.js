import { Task } from "./task.js";

export class TaskList {

    constructor(){
        this.tasks = [];
    }

    add(taskText){
        const t = new Task(taskText);
        this.tasks.push(t);
    }

    remove(index){
        this.tasks.splice(index, 1)
    }

    getTask(index){
        return this.tasks[index];
    }
}