import { list, taskTemplate } from "./elements.js";

export function renderList(taskList){
    list.innerHTML = null;
    taskList.tasks.forEach((task, index) => {
        const elem = taskTemplate.content.cloneNode(true);
        const txt = elem.querySelector('.task-text')
        txt.textContent = task.text;

        if (task.isDone){
            txt.style.textDecoration = 'line-through';
        }
        
        elem.querySelector('.task-container').setAttribute('data-index', index)
        list.append(elem);
    });
}