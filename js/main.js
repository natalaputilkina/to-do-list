import { TaskList } from "./taskList.js";
import { renderList } from "./utils.js";
import { form, list } from "./elements.js";


const tl = new TaskList();

form.addEventListener('submit', (event) => {
    event.preventDefault();
    const value = event.target.elements.taskText.value;
    tl.add(value);
    renderList(tl);
})


list.addEventListener('click', event => {

    if (!event.target.classList.contains('task-remove')){
        return;
    }

    const index = event.target.closest('.task-container').dataset.index;
    tl.remove(index)
    renderList(tl);


})

list.addEventListener('click', event => {

    if (!event.target.classList.contains('task-done')){
        return;
    }
    
    const index = event.target.closest('.task-container').dataset.index;
    tl.getTask(index).markAsDone()
    renderList(tl);


})


