export class Task {
    constructor(taskText){
        this.text = taskText
        this.isDone = false;
    }

    markAsDone(){
        this.isDone = true;
    }
}