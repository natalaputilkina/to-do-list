const form = document.forms.addTaskForm;

const list = document.querySelector('.task-list');
const taskTemplate = document.getElementById('taskTemplate');

export {
    form, list, taskTemplate
};

